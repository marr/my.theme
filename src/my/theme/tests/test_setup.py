# -*- coding: utf-8 -*-
"""Setup tests for this package."""
from plone import api
from my.theme.testing import MY_THEME_INTEGRATION_TESTING  # noqa

import unittest


class TestSetup(unittest.TestCase):
    """Test that my.theme is properly installed."""

    layer = MY_THEME_INTEGRATION_TESTING

    def setUp(self):
        """Custom shared utility setup for tests."""
        self.portal = self.layer['portal']
        self.installer = api.portal.get_tool('portal_quickinstaller')

    def test_product_installed(self):
        """Test if my.theme is installed."""
        self.assertTrue(self.installer.isProductInstalled(
            'my.theme'))

    def test_browserlayer(self):
        """Test that IMyThemeLayer is registered."""
        from my.theme.interfaces import (
            IMyThemeLayer)
        from plone.browserlayer import utils
        self.assertIn(IMyThemeLayer, utils.registered_layers())


class TestUninstall(unittest.TestCase):

    layer = MY_THEME_INTEGRATION_TESTING

    def setUp(self):
        self.portal = self.layer['portal']
        self.installer = api.portal.get_tool('portal_quickinstaller')
        self.installer.uninstallProducts(['my.theme'])

    def test_product_uninstalled(self):
        """Test if my.theme is cleanly uninstalled."""
        self.assertFalse(self.installer.isProductInstalled(
            'my.theme'))

    def test_browserlayer_removed(self):
        """Test that IMyThemeLayer is removed."""
        from my.theme.interfaces import \
            IMyThemeLayer
        from plone.browserlayer import utils
        self.assertNotIn(IMyThemeLayer, utils.registered_layers())
